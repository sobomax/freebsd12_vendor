# $FreeBSD: 3c8727ba2d4597003f50acdebb50c8addcd0cd90 $

FORTUNE_SRC=	${SRCTOP}/usr.bin/fortune
FORTUNE_OBJ=	${OBJTOP}/usr.bin/fortune

.include "${SRCTOP}/usr.bin/Makefile.inc"
