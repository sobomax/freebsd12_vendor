/* $FreeBSD: 1915720087e6046c7ef9eeff019a766a39658f28 $ */

#define	TTYMSG_IOV_MAX	32

const char	*ttymsg(struct iovec *, int, const char *, int);
