/* Copyright Red Hat Inc 2010.
 * Author : Dave Airlie <airlied@redhat.com>
 */
#include <sys/cdefs.h>
__FBSDID("$FreeBSD: 5602c946d2308136476f9a76a3db63130e55162e $");

#include <drm/drmP.h>
#include <drm/radeon_drm.h>
#include "radeon.h"

#define CREATE_TRACE_POINTS
#include "radeon_trace.h"
