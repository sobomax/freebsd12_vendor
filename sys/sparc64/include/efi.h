/*-
 * This file is in the public domain since it's just boilerplate.
 *
 * $FreeBSD: 1f7ad0da18afa04ce9eef4388fdf933f90d29a89 $
 */

#ifndef __SPARC64_INCLUDE_EFI_H_
#define __SPARC64_INCLUDE_EFI_H_

#define	EFIABI_ATTR

/* Note: we don't actually support this on sparc64 */

#endif /* __I386_INCLUDE_EFI_H_ */
