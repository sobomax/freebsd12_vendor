/*
 * This trivial work is released to the public domain, or licensed under the
 * terms of the CC0, at your option.
 * $FreeBSD: d13b41da4be2bfcee5fed1d518dbc23446fc26f5 $
 */
#pragma once

typedef struct crypto_session	*crypto_session_t;
