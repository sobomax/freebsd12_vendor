/*-
 * This file is in the public domain since it's just boilerplate.
 *
 * $FreeBSD: 02508f4d5188cf4cf0caeaaf719ed40e7469ecf0 $
 */

#ifndef __I386_INCLUDE_EFI_H_
#define __I386_INCLUDE_EFI_H_

#define	EFIABI_ATTR /* __attribute__((ms_abi)) */ /* clang fails with this */

/* Note: we don't actually support this on i386 yet */

#endif /* __I386_INCLUDE_EFI_H_ */
