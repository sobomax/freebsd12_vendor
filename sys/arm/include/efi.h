/*-
 * This file is in the public domain since it's just boilerplate.
 *
 * $FreeBSD: 02e1e7e975eff96945c6d671c534b67a8cde4254 $
 */

#ifndef __ARM_INCLUDE_EFI_H_
#define __ARM_INCLUDE_EFI_H_

#define	EFIABI_ATTR

#endif /* __ARM_INCLUDE_EFI_H_ */
