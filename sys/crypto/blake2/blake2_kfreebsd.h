/* This file is in the public domain */
/* $FreeBSD: cc59aee757c972404ca76d1df4074274dca5fdfb $ */

#pragma once

#include <sys/param.h>
#include <sys/systm.h>	/* memcpy, memset */
