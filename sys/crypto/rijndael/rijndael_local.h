/*	$KAME: rijndael_local.h,v 1.5 2003/08/28 08:37:24 itojun Exp $	*/
/*	$FreeBSD: 7c765a2b11f8d75beded08f2cbb428e0de790a1e $	*/

/* the file should not be used from outside */
typedef u_int8_t		u8;
typedef u_int16_t		u16;
typedef u_int32_t		u32;
