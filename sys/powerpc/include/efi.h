/*-
 * This file is in the public domain since it's just boilerplate.
 *
 * $FreeBSD: f4b97d62b3e099cff55ffd15dba60b989ff4f767 $
 */

#ifndef __POWERPC_INCLUDE_EFI_H_
#define __POWERPC_INCLUDE_EFI_H_

#define	EFIABI_ATTR

/* Note: we don't actually support this on powerpc */

#endif /* __POWERPC_INCLUDE_EFI_H_ */
