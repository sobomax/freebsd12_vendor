/* $FreeBSD: 9bead018bcec168e0be8af404039106d213810be $ */

#ifndef	_SOLARIS_H_
#define	_SOLARIS_H_

#include <sys/ccompile.h>

#include <fcntl.h>

#define	NOTE(s)

int mkdirp(const char *, mode_t);

#endif	/* !_SOLARIS_H_ */
