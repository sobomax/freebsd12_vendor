/*-
 *
 * This file is in the public domain.
 */
/* $FreeBSD: 4c771d79769fd85a848d2098a1f400415853fea2 $ */

#pragma once

#include <lua.h>

int luaopen_posix_unistd(lua_State *L);
