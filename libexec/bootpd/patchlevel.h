/* 
 * patchlevel.h 
 *
 * $FreeBSD: fc79f18b4068c95ec96ebea911299198ad5ab5c3 $
 */

#define VERSION 	"2.4"
#define PATCHLEVEL	3
