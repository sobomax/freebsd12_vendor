/*
 * $FreeBSD: 0e9c668b4444444b437d7014ac0e3d8444da447e $
 */

int	lm_init (char *);
void	lm_fini (void);
char *	lm_find (const char *, const char *);
char *	lm_findn (const char *, const char *, const size_t);
