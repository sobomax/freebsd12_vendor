# $FreeBSD: 6380db6c265ac7793d55cc1f1a8b825eefba5bfe $

SRCS+=	_ctx_start.S \
	fabs.S \
	flt_rounds.c \
	infinity.c \
	ldexp.c \
	makecontext.c \
	_setjmp.S \
	_set_tp.c \
	setjmp.S \
	sigsetjmp.S \
	trivial-getcontextx.c
