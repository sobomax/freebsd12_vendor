# $FreeBSD: 266446c9d13281d8b736a8d8b149aaf7af5baadc $

MDSRCS+= \
	bcmp.S \
	bcopy.S \
	bzero.S \
	ffs.S \
	memcpy.S \
	memmove.S \
	strchr.S \
	strcmp.S \
	strlen.S \
	strrchr.S
