# $FreeBSD: db88ac723539ffd32608d7fdd6560798449e1ca4 $

MDSRCS+= \
	bcmp.S \
	memcmp.S \
	memcpy.S \
	memmove.S \
	memset.S \
	strcat.S \
	strcmp.S \
	stpcpy.S
