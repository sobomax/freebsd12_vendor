/* $FreeBSD: dabbfd7f7ab428ff52995c3b0709ad468600aff7 $ */
#define	charXX_t	char32_t
#define	cXXrtomb	c32rtomb
#define	cXXrtomb_l	c32rtomb_l
#define	SRCBUF_LEN	1
#define	UTF_XX_INTERNAL	"UTF-32-INTERNAL"

#include "cXXrtomb_iconv.h"
