/* $FreeBSD: 86bd9dab2a52d480b0e69cf2e90e5bb982a3c099 $ */
#define	charXX_t	char16_t
#define	cXXrtomb	c16rtomb
#define	cXXrtomb_l	c16rtomb_l
#define	SRCBUF_LEN	2
#define	UTF_XX_INTERNAL	"UTF-16-INTERNAL"

#include "cXXrtomb_iconv.h"
