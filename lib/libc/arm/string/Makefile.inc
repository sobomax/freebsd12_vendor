# $FreeBSD: cf3175ea4e8cbdab425a56476f239aeb40267143 $

MDSRCS+= \
	bcopy.S \
	bzero.S \
	ffs.S \
	memcmp.S \
	memcpy.S \
	memmove.S \
	memset.S \
	strcmp.S \
	strlen.S \
	strncmp.S
