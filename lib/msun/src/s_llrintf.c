#include <sys/cdefs.h>
__FBSDID("$FreeBSD: 7ec6015238d315334e4db54efabd78fc7715f763 $");

#define type		float
#define	roundit		rintf
#define dtype		long long
#define	fn		llrintf

#include "s_lrint.c"
