#include <sys/cdefs.h>
__FBSDID("$FreeBSD: 7c959ec950ae9245348e4366bc65913e7a50421c $");

#define type		double
#define	roundit		rint
#define dtype		long long
#define	fn		llrint

#include "s_lrint.c"
