/* $FreeBSD: e4549103209bca9cde05578c0f31dc453ef12506 $ */
#define LLVM_REVISION "llvmorg-10.0.1-0-gef32c611aa2"
#define LLVM_REPOSITORY "git@github.com:llvm/llvm-project.git"
