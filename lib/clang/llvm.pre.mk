
# $FreeBSD: 774d7d47ea3772401434291bee4c52d39a9c894b $

LLVM_BASE=	${SRCTOP}/contrib/llvm-project
LLVM_SRCS=	${LLVM_BASE}/llvm

LLVM_TBLGEN?=	llvm-tblgen
