/* File in public domain */
/* Brings in the glue for UEFI/EDK2 Tianocore code to run on this OS */
/* $FreeBSD: ecba6874b5fe7891f98eea093330538120a8f758 $ */
#include "efi-osdep.h"
