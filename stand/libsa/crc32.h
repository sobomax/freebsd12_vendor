/*-
 *  COPYRIGHT (C) 1986 Gary S. Brown.  You may use this program, or
 *  code or tables extracted from it, as desired without restriction.
 *
 * $FreeBSD: adfd628671aac676cc4dc909ac168f242ca4527d $
 */

#ifndef _CRC32_H_
#define	_CRC32_H_

uint32_t crc32(const void *buf, size_t size);

#endif	/* !_CRC32_H_ */
