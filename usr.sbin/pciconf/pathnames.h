/* $FreeBSD: 61c0993bbb7c42627ed941894fa0fe270e10af57 $ */
#define	_PATH_DEVPCI	"/dev/pci"
#define	_PATH_PCIVDB	"/usr/share/misc/pci_vendors"
#define	_PATH_LPCIVDB	"/usr/local/share/pciids/pci.ids"
