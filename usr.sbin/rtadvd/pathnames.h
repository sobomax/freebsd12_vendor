/*	$KAME: pathnames.h,v 1.2 2000/05/16 13:34:13 itojun Exp $	*/
/* 	$FreeBSD: 248ee19f66e0a8d9f12d9d487b7728d93e74b705 $	*/

#define	_PATH_RTADVDCONF "/etc/rtadvd.conf"
#define	_PATH_RTADVDPID "/var/run/rtadvd.pid"
#define	_PATH_CTRL_SOCK	"/var/run/rtadvd.sock"
