/* $FreeBSD: e1fa341e0c782d8b33f97a102e956c1d71a0eadc $ */

#define KEYMAP_PATH	"/usr/share/syscons/keymaps/"
#define FONT_PATH	"/usr/share/syscons/fonts/"
#define SCRNMAP_PATH	"/usr/share/syscons/scrnmaps/"

#define VT_KEYMAP_PATH	"/usr/share/vt/keymaps/"
#define VT_FONT_PATH	"/usr/share/vt/fonts/"
