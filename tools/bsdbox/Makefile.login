#
# This builds login and friends.
#
# $FreeBSD: a4574ec0abc4f34de5fefc119e57ff979ee99868 $
#

CRUNCH_PROGS_libexec+=	getty
CRUNCH_PROGS_usr.bin+=	cap_mkdb
CRUNCH_PROGS_usr.sbin+=	pwd_mkdb
