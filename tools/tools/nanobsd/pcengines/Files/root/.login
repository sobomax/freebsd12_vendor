# $FreeBSD: 196a640f28e38e49eaea9afed6293f66d95e978c $
#
# .login - csh login script, read by login shell, after `.cshrc' at login.
#
# see also csh(1), environ(7).
#

# Uncomment to display a random cookie each login:
# [ -x /usr/bin/fortune ] && /usr/bin/fortune -s
