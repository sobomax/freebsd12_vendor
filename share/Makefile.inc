#	@(#)Makefile.inc	8.1 (Berkeley) 6/5/93
# $FreeBSD: dbe7ccfdebca682c065be3b7f6bd4cc2ca87c024 $

BINDIR?=	${SHAREDIR}
BINOWN=		${SHAREOWN}
BINGRP=		${SHAREGRP}
