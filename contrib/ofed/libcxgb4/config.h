/* $FreeBSD: 5479189dc5682e0c743024834402dda264bc4bdf $ */

#define	ENODATA ECONNREFUSED
#define	likely(x) __predict_true(x)
#define	unlikely(x) __predict_false(x)

